package com.mid.aniTweets.accessor;

import com.mid.aniTweets.scroll.accessor.AnimeMapAccessor;
import com.mid.aniTweets.scroll.model.AnimeSchema;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;


/**
 * Created by mid on 14/04/04.
 */
public class AnimeMapAccessorTest {

    @Test
    public void アニメ一覧の取得で取得に成功した場合正常終了すること() throws IOException {
        List<AnimeSchema> res = new AnimeMapAccessor().searchOnAirLists();
        assertThat(res).isNotNull();
        assertThat(res.size()).isGreaterThan(0);
    }

    @Test
    public void アニメ一覧の取得で取得に失敗した場合nullを返却すること() throws IOException {
        AnimeMapAccessor accessor = spy(new AnimeMapAccessor());
        doReturn("http://dummy.not.exists.com").when(accessor).getAnimeMapUrl();
        List<AnimeSchema> res = accessor.searchOnAirLists();
        assertThat(res).isNull();
    }

}
