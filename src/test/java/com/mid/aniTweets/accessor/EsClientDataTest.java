package com.mid.aniTweets.accessor;

import com.mid.aniTweets.scroll.accessor.EsClient;
import com.mid.aniTweets.scroll.accessor.EsClientException;
import com.mid.aniTweets.scroll.utils.Config;
import net.sf.json.JSONObject;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.io.FileSystemUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.elasticsearch.node.NodeBuilder.nodeBuilder;
import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

/**
 * Created by mid on 14/04/10.
 */
public class EsClientDataTest {
    private static final String TEST_INDEX = "test_index";
    private static final String TEST_CLUSTER_NAME = "test_cluster";

    private Client testNodeClient;
    private EsClient esClient;
    private Config testConfig;

    @Before
    public void before() throws Exception {
        // テスト用NodeClientの作成
        this.testNodeClient = nodeBuilder()
                .clusterName(TEST_CLUSTER_NAME)
                .node().client();

        // テスト用のConfigインスタンス作成
        Config config = spy(new Config());
        doReturn(TEST_CLUSTER_NAME).when(config).getEsClusterName();

        // テスト用インスタンスの作成
        this.esClient = EsClient.getTransportClient(config);
    }

    @After
    public void after() {
        this.esClient.close();
        FileSystemUtils.deleteRecursively(new File("./data/" + TEST_CLUSTER_NAME), true);
        if(this.testNodeClient != null) {
            this.testNodeClient.close();
        }
    }

    @Test
    public void 指定したクエリに完全一致するデータが存在する場合ドキュメントのIDを取得できること() throws EsClientException {
        this.esClient.createDocument(TEST_INDEX, "testType", "{\"sample\":\"value\"}");
        String field = "sample";
        String value = "value";
        String result = this.esClient.getDocumentId(TEST_INDEX, "testType", field, value);
        assertThat(result).isNotNull();
        assertThat(result).isNotEmpty();
    }

//    @Test
//    public void 指定した日本語クエリに完全一致するデータが存在する場合ドキュメントのIDを取得できること() throws EsClientException {
//        this.esClient.createDocument(TEST_INDEX, "testType", "{\"sample\":\"日本語\"}");
//        String field = "sample";
//        String value = "日本語";
//        String result = this.esClient.getDocumentId(TEST_INDEX, "testType", field, value);
//        assertThat(result).isNotNull();
//        assertThat(result).isNotEmpty();
//    }

    @Test
    public void TwitterRiverの動作確認() throws EsClientException {
        JSONObject filterQuery = new JSONObject();
        filterQuery.put("tracks", "星刻の竜騎士(ドラグナー), マンガ家さんとアシスタントさんと, ラブライブ！(第2期), M3-ソノ黒キ鋼-, 神々の悪戯(かみがみのあそび), M3-ソノ黒キ鋼-, 魔法少女大戦, ブラック・ブレット, ブラック・ブレット, 史上最強の弟子ケンイチ 闇の襲撃, ブラック・ブレット, ソウルイーターノット！, 星刻の竜騎士(ドラグナー), 一週間フレンズ。, ノーゲーム・ノーライフ, 棺姫(ひつぎ)のチャイカ, デート・ア・ライブII, 風雲維新ダイショーグン, 彼女がフラグをおられたら, ソウルイーターノット！, ノーゲーム・ノーライフ, エスカ＆ロジーのアトリエ～黄昏の空の錬金術士～, ご注文はうさぎですか？, ピンポン, 龍ヶ嬢七々々の埋蔵金, 監督不行届, ご注文はうさぎですか？, 棺姫(ひつぎ)のチャイカ, エスカ＆ロジーのアトリエ～黄昏の空の錬金術士～, 蟲師 続章, ジョジョの奇妙な冒険 第三部 スターダストクルセイダース, selector infected WIXOSS(セレクター インフェクテッド ウィクロス), 犬神さんと猫山さん, 棺姫(ひつぎ)のチャイカ, 牙狼〈GARO〉-魔戒ノ花-, デート・ア・ライブII, 風雲維新ダイショーグン, 牙狼〈GARO〉-魔戒ノ花-, テンカイナイト, テンカイナイト, メカクシティアクターズ, 魔法科高校の劣等生, 神々の悪戯(かみがみのあそび), selector infected WIXOSS(セレクター インフェクテッド ウィクロス), ベイビーステップ, 極黒のブリュンヒルデ, ラブライブ！(第2期), ブレイク ブレイド, キャプテン・アース, 一週間フレンズ。, デート・ア・ライブII, 彼女がフラグをおられたら, 風雲維新ダイショーグン, 魔法科高校の劣等生, 健全ロボ ダイミダラー, キャプテン・アース, 魔法科高校の劣等生");
        this.esClient.createTwitterRiver(filterQuery);
        this.esClient.deleteRiver();
    }

}

