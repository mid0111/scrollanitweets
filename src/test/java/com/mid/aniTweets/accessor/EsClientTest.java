package com.mid.aniTweets.accessor;

import com.mid.aniTweets.scroll.accessor.EsClient;
import com.mid.aniTweets.scroll.accessor.EsClientException;
import com.mid.aniTweets.scroll.utils.Config;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.fest.assertions.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

/**
 * Created by mid on 14/04/09.
 */
public class EsClientTest {

    private Config defaultConfig = new Config();

    @Test
    public void インスタンスを生成できること() throws EsClientException {
        EsClient client = EsClient.getTransportClient(defaultConfig);
        assertThat(client).isNotNull();
    }

    @Test(expected = EsClientException.class)
    public void 不正なポートを指定した場合ドキュメント生成でEsClientExceptionがthrowされること() throws Exception {
        // 不正なホストを返却するConfigインスタンスを生成
        Config config = spy(new Config());
        doReturn(999999).when(config).getEsPort();

        // テスト
        EsClient client = EsClient.getTransportClient(config);
    }

    @Test(expected = EsClientException.class)
    public void 存在しないホストを指定した場合ドキュメント生成でEsClientExceptionがthrowされること() throws Exception {
        // 不正なホストを返却するConfigインスタンスを生成
        Config config = spy(new Config());
        List<String> dummyHosts = new ArrayList<String>();
        dummyHosts.add("192.168.255.255");
        doReturn(dummyHosts).when(config).getEsHosts();

        // テスト
        EsClient client = EsClient.getTransportClient(config);
        client.createDocument("testindex", "testType", "{\"sample\":\"value\"}");
    }

    @Test(expected = EsClientException.class)
    public void 存在しないクラスタ名指定した場合ドキュメント生成でEsClientExceptionがthrowされること() throws Exception {
        // 不正なホストを返却するConfigインスタンスを生成
        Config config = spy(new Config());
        doReturn("dummyClusterName").when(config).getEsClusterName();

        // テスト
        EsClient client = EsClient.getTransportClient(config);
        client.createDocument("testindex", "testType", "{\"sample\":\"value\"}");
    }
}
