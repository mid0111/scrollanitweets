package com.mid.aniTweets.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mid.aniTweets.scroll.model.AnimeSchema;
import com.mid.aniTweets.scroll.model.AnimeSchemas;
import org.junit.Test;

import java.io.IOException;

import static org.fest.assertions.Assertions.assertThat;

/**
 * Created by mid on 14/04/05.
 */
public class AnimeSchemaTest {

    @Test
    public void JSON文字列からインスタンスを生成できること() throws IOException {
        String jsonStr = "{" +
                "\"cable\":\"1\"," +
                "\"episode\":\"?\"," +
                "\"next\":\"2014-04-07\"," +
                "\"state\":\"new\"," +
                "\"station\":\"MX\"," +
                "\"time\":\"24:30\"," +
                "\"title\":\"星刻の竜騎士(ドラグナー)\"," +
                "\"today\":\"0\"," +
                "\"url\":\"http://animemap.net/time/title/1878/\"," +
                "\"week\":\"月曜日\"" +
                "}";
        ObjectMapper mapper = new ObjectMapper();
        AnimeSchema animeSchema = mapper.readValue(jsonStr, AnimeSchema.class);
        assertThat(animeSchema.getCable()).isNotNull();
        assertThat(animeSchema.getEpisode()).isNotNull();
        assertThat(animeSchema.getNext()).isNotNull();
        assertThat(animeSchema.getState()).isNotNull();
        assertThat(animeSchema.getStation()).isNotNull();
        assertThat(animeSchema.getTime()).isNotNull();
        assertThat(animeSchema.getTitle()).isNotNull();
        assertThat(animeSchema.getToday()).isNotNull();
        assertThat(animeSchema.getUrl()).isNotNull();
        assertThat(animeSchema.getWeek()).isNotNull();
    }

    @Test
    public void AnimeSchemasインスタンスにAnimeSchemaインスタンスを追加できること() throws IOException {
        String jsonStr = "{" +
                "\"cable\":\"1\"," +
                "\"episode\":\"?\"," +
                "\"next\":\"2014-04-07\"," +
                "\"state\":\"new\"," +
                "\"station\":\"MX\"," +
                "\"time\":\"24:30\"," +
                "\"title\":\"星刻の竜騎士(ドラグナー)\"," +
                "\"today\":\"0\"," +
                "\"url\":\"http://animemap.net/time/title/1878/\"," +
                "\"week\":\"月曜日\"" +
                "}";
        ObjectMapper mapper = new ObjectMapper();
        AnimeSchema animeSchema = mapper.readValue(jsonStr, AnimeSchema.class);
        AnimeSchemas animes = new AnimeSchemas();
        animes.add(animeSchema);
        assertThat(animes.size()).isEqualTo(1);
        assertThat(animes.get(0).getTitle()).isEqualTo("星刻の竜騎士(ドラグナー)");
        for(AnimeSchema anime : animes) {
            assertThat(anime.getTitle()).isEqualTo("星刻の竜騎士(ドラグナー)");
        }
    }
}
