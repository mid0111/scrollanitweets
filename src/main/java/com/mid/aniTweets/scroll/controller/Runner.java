package com.mid.aniTweets.scroll.controller;

import com.mid.aniTweets.scroll.accessor.AnimeMapAccessor;
import com.mid.aniTweets.scroll.accessor.EsClient;
import com.mid.aniTweets.scroll.accessor.EsClientException;
import com.mid.aniTweets.scroll.model.AnimeSchema;
import com.mid.aniTweets.scroll.model.AnimeSchemas;
import com.mid.aniTweets.scroll.utils.Config;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by mid on 14/04/09.
 */
public class Runner {

    static Logger log = LoggerFactory.getLogger(Runner.class);
    private Config config = new Config();

    public void run() {

        // 現在放映中のアニメ一覧を取得.
        AnimeMapAccessor animeMapAccessor = new AnimeMapAccessor();
        AnimeSchemas animes = animeMapAccessor.searchOnAirLists();
        if(animes == null) {
            log.error("Failed to get anime map.");
            return;
        }

        // DBにアニメ一覧を保存
        EsClient client = null;
        try {
            client = EsClient.getTransportClient(config);

            for(AnimeSchema anime : animes) {
                // 登録データに更新日を付加
                JSONObject document = anime.getSource();
                document.put("_updated", anime.getUpdated());

                String id = client.getDocumentId("anime", "anime", "title", anime.getTitle());

                // すでに登録済みのデータの場合は更新日のみ更新する
                if(id != null) {
                    client.updateDocument("anime", "anime", document.toString(), id);
                } else {
                    // 新規登録
                    document.put("_created", anime.getUpdated());
                    client.createDocument("anime", "anime", document.toString());
                }
            }
        } catch (EsClientException e) {
            log.info("Failed to regist data.");
            return;
        }

        // Twitter Riverの設定更新
        try {
        }finally {
            client.close();
        }
    }

}
