package com.mid.aniTweets.scroll;

import com.mid.aniTweets.scroll.controller.Runner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Applicationメインクラス.
 * Created by mid on 14/04/02.
 */
public class App {
    static Logger log = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        log.info("App Started. -----------------------------");
        try {
            Runner runner = new Runner();
            runner.run();
        } finally {
            log.info("App End. ---------------------------------");
        }
    }

}

