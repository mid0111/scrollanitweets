package com.mid.aniTweets.scroll.accessor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by mid on 14/04/09.
 */
public class EsClientException extends Exception {

    static Logger log = LoggerFactory.getLogger(EsClient.class);

    public EsClientException() {
        super();
    }

    public EsClientException(Throwable t) {
        super(t);
        log.error(t.getMessage(), t);
    }
}
