package com.mid.aniTweets.scroll.accessor;

import com.mid.aniTweets.scroll.utils.Config;
import net.sf.json.JSONObject;
import org.elasticsearch.ElasticSearchException;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Requests;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by mid on 14/04/09.
 */
public class EsClient {

    private static Logger log = LoggerFactory.getLogger(EsClient.class);
    private TransportClient client = null;

    private EsClient() {
    }

    public static EsClient getTransportClient(Config config) throws EsClientException {
        try {
            String clusterName = config.getEsClusterName();
            int esPort = config.getEsPort();

            // クラスタ名の設定
            Settings settings = ImmutableSettings.settingsBuilder()
                    .put("cluster.name", clusterName).build();

            // インスタンス生成
            TransportClient client = new TransportClient(settings);

            // Elasticsearchホストの追加
            for (Object o : config.getEsHosts()) {
                String esHost = o.toString();
                client.addTransportAddress(new InetSocketTransportAddress(esHost, esPort));
            }

            EsClient esClient = new EsClient();
            esClient.client = client;
            return esClient;
        } catch(Exception e) {
            throw new EsClientException(e);
        }
    }

    public String getDocumentId(String index, String type, String field, String value) throws EsClientException {
        String id = null;
        SearchResponse response = this.client.prepareSearch(index)
                .setTypes(type)
                .setQuery(QueryBuilders.termQuery(field, value))
                .execute().actionGet();
        if(response.getHits().totalHits() > 1) {
            throw new EsClientException();
        }
        for (SearchHit hit : response.getHits().getHits()) {
            id = hit.getId();
        }
        return id;
    }

    public void createDocument(String index, String type, String document) throws EsClientException {
        IndexResponse response = null;
        try {
            response = this.client.prepareIndex(index, type).setSource(document).setRefresh(true).execute().actionGet();
            log.debug("Document created. index:[" + response.getIndex() + "] type:[" + response.getType() + "] id:[" + response.getId());
        } catch (ElasticSearchException e) {
            throw new EsClientException(e);
        }
    }

    public void updateDocument(String index, String type, String document, String id) throws EsClientException {
    }

    public void createTwitterRiver(JSONObject twitterFilterQuery) {
        Config config = new Config();

        // Twitter OAuth
        JSONObject twitterOAuthSettings = new JSONObject();
        twitterOAuthSettings.put("consumer_key", config.getTwitterCousumerKey());
        twitterOAuthSettings.put("consumer_secret", config.getTwitterCousumerSecret());
        twitterOAuthSettings.put("access_token", config.getTwitterAccessToken());
        twitterOAuthSettings.put("access_token_secret", config.getTwitterAccessTokenSecret());

        // Twitter Settings
        JSONObject twitterSettings = new JSONObject();
        twitterSettings.put("oauth", twitterOAuthSettings);
        twitterSettings.put("filter", twitterFilterQuery);

        // Index Settings
        JSONObject indexSettings = new JSONObject();
        indexSettings.put("index", "twitter_river");
        indexSettings.put("type", "tweets");
        indexSettings.put("bulk_size", 100);
        indexSettings.put("flush_interval", "5s");

        // Construct request source
        JSONObject source = new JSONObject();
        source.put("type", "twitter");
        source.put("twitter", twitterSettings);
        source.put("index", indexSettings);

        client.index(Requests.indexRequest("_river")
                .type("my_river")
                .id("_meta")
                .source(source)).actionGet();
    }

    public void deleteRiver() {
        client.delete(Requests.deleteRequest("_river").type("my_river").id("_meta")).actionGet();
        client.delete(Requests.deleteRequest("_river").type("my_river").id("_status")).actionGet();
    }

    public void close() {
        if(this.client != null) {
            this.client.close();
        }
    }

}
