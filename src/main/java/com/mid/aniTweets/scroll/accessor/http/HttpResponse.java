package com.mid.aniTweets.scroll.accessor.http;

import net.sf.json.JSONObject;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by mid on 14/04/03.
 */
public class HttpResponse {

    Logger log = LoggerFactory.getLogger(HttpResponse.class);

    private final Header[] headers;
    private final StatusLine statusLine;
    String body;
    private int code;

    public HttpResponse(CloseableHttpResponse response) throws IOException {
        this.headers = response.getAllHeaders();
        this.statusLine = response.getStatusLine();
        this.body = bodyToString(response.getEntity());
        log.info("code: " + this.getCode());
        log.info("Response: --------");
        for (Header header : headers) {
            log.info(header.getName() + ": " + header.getValue());
        }
        log.info(this.body);
        log.info("------------------");
    }

    private String bodyToString(HttpEntity entity) throws IOException {
        InputStream inputStream = entity.getContent();

        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        String response = sb.toString();

        EntityUtils.consume(entity);
        return response;
    }

    public String body() {
        return this.body;
    }

    public JSONObject bodyAsJson() throws IOException {
        JSONObject json = JSONObject.fromObject(this.body);
        JSONObject response = (JSONObject) json.get("response");
        return response;
    }

    public int getCode() {
        return this.statusLine.getStatusCode();
    }
}
