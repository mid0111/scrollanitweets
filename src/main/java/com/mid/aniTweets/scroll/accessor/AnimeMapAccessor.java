package com.mid.aniTweets.scroll.accessor;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.mid.aniTweets.scroll.accessor.http.HttpClient;
import com.mid.aniTweets.scroll.accessor.http.HttpResponse;
import com.mid.aniTweets.scroll.model.AnimeSchema;
import com.mid.aniTweets.scroll.model.AnimeSchemas;
import com.mid.aniTweets.scroll.utils.Config;
import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Created by mid on 14/04/02.
 */
public class AnimeMapAccessor {

    private Logger log = LoggerFactory.getLogger(AnimeMapAccessor.class);

    public AnimeSchemas searchOnAirLists() {
        HttpClient httpClient = new HttpClient(getAnimeMapUrl());
        HttpResponse httpResponse = null;
        try {
            httpResponse = httpClient.get();
        } catch (IOException e) {
            log.error("Failed to connect animemap service.", e);
            return null;
        }

        JSONObject res = null;
        try {
            res = httpResponse.bodyAsJson();
        } catch (IOException | JSONException e) {
            log.error("Failed to parse animemap response.", e);
            return null;
        }

        log.info(res.toString());

        AnimeSchemas animes = null;
        try {
            animes = getAnimes(res);
        } catch (IOException e) {
            log.error("Failed to parse animemap response.", e);
        }
        return animes;
    }

    private AnimeSchemas getAnimes(JSONObject res) throws IOException {
        AnimeSchemas animes = new AnimeSchemas();
        JSONArray items = JSONArray.fromObject(res.get("item"));
        for(Object item : items) {
            JSONObject json = (JSONObject.fromObject(item));
            ObjectMapper mapper = new ObjectMapper();
            animes.add(mapper.readValue(json.toString(), AnimeSchema.class));
        }
        return animes;
    }

    public String getAnimeMapUrl() {
        return new Config().getAniTweetsUrl();
    }
}
