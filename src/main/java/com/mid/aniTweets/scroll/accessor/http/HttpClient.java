package com.mid.aniTweets.scroll.accessor.http;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;

/**
 * Created by mid on 14/04/02.
 */
public class HttpClient {

    private final String url;

    public HttpClient(String url) {
        this.url = url;
    }

     public HttpResponse get() throws IOException {
         CloseableHttpClient httpclient = HttpClients.createDefault();
         HttpGet httpGet = new HttpGet(url);
         CloseableHttpResponse response = httpclient.execute(httpGet);

         return new HttpResponse(response);
     }

}
