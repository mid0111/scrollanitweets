package com.mid.aniTweets.scroll.utils;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by mid on 14/04/03.
 */
public class Config {

    Logger log = LoggerFactory.getLogger(Config.class);
    private static final String KEY_ROOT = "com.mid.aniTweets.";
    private PropertiesConfiguration config;

    public Config() {
        try {
            this.config = new PropertiesConfiguration("aniTweets.config");
        } catch (ConfigurationException e) {
            log.error("Failed to load configuration.", e);
        }
    }

    public String getAniTweetsUrl() {
        return this.config.getString(KEY_ROOT + "anime.list.source.url");
    }

    public List<Object> getEsHosts() {
        return this.config.getList(KEY_ROOT + "elasticsearch.hosts");
    }

    public int getEsPort() {
        return this.config.getInt(KEY_ROOT + "elasticsearch.port");
    }

    public String getEsClusterName() {
        return this.config.getString(KEY_ROOT + "elasticsearch.cluster.name");
    }

    public String getTwitterCousumerKey() {
        return this.config.getString(KEY_ROOT + "consumer_key");
    }

    public String getTwitterCousumerSecret() {
        return this.config.getString(KEY_ROOT + "consumer_secret");
    }

    public String getTwitterAccessToken() {
        return this.config.getString(KEY_ROOT + "access_token");
    }

    public String getTwitterAccessTokenSecret() {
        return this.config.getString(KEY_ROOT + "access_token_secret");
    }

}
