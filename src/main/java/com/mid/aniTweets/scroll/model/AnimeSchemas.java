package com.mid.aniTweets.scroll.model;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mid on 14/04/08.
 */

public class AnimeSchemas extends AbstractList<AnimeSchema> {

    private List<AnimeSchema> list = null;

    public AnimeSchemas() {
        list = new ArrayList<AnimeSchema>();
    }

    @Override
    public AnimeSchema get(int index) {
        return list.get(index);
    }

    @Override
    public boolean add(AnimeSchema e) {
        list.add(list.size(), e);
        return true;
    }

    @Override
    public int size() {
        return list.size();
    }
}
