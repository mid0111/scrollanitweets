package com.mid.aniTweets.scroll.model;

import net.sf.json.JSONObject;

import java.util.Date;

/**
 * Created by mid on 14/04/04.
 */
public class AnimeSchema {

    private final long updated;
    private String cable;
    private String episode;
    private String next;
    private String date;
    private String state;
    private String station;
    private String time;
    private String title;
    private String today;
    private String url;
    private String week;

    public AnimeSchema() {
        this.updated = new Date().getTime();
    }
    public String getCable() {
        return cable;
    }

    public void setCable(String cable) {
        this.cable = cable;
    }

    public String getEpisode() {
        return episode;
    }

    public void setEpisode(String episode) {
        this.episode = episode;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getToday() {
        return today;
    }

    public void setToday(String today) {
        this.today = today;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }

    public JSONObject getSource() {
        JSONObject json = new JSONObject();
        json.put("title", getTitle());

        return json;
    }

    public long getUpdated() {
        return updated;
    }
}
