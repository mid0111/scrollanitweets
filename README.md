
#### Configuration

```bash
cat > src/main/resources/
# on air anime list source
com.mid.aniTweets.anime.list.source.url=http://animemap.net/api/table/shizuoka.json

# elasticsearch
com.mid.aniTweets.elasticsearch.hosts=localhost
com.mid.aniTweets.elasticsearch.port=9300
#com.mid.aniTweets.elasticsearch.cluster.name=clusterAniTweets
com.mid.aniTweets.elasticsearch.cluster.name=elasticsearch

# Twitter oAuth
com.mid.aniTweets.twitter.consumer_key={twitter consumer key}
com.mid.aniTweets.twitter.consumer_secret={twitter consumer secret}
com.mid.aniTweets.twitter.access_token={twitter access token}
com.mid.aniTweets.twitter.access_token_secret={twitter access token secret}
```
